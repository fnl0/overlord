BUILD_VERSION=$1

echo "Unpacking $BUILD_VERSION...."
cd frontend
tar xzf "./$BUILD_VERSION.tar.gz"
rm "./$BUILD_VERSION.tar.gz"
ln -s "$PWD/$BUILD_VERSION" "$PWD/current.new"
mv -T "$PWD/current.new" "$PWD/current"