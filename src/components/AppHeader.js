import { withTheme } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import React from 'react';
import { css } from 'react-emotion';
import TextLink from './TextLink';
import AppLogo from './AppLogo';
import AppHeaderMenu from './AppHeaderMenu';

const hoc = BaseComponent =>
  withWidth()(withTheme()(BaseComponent));

export default hoc(function AppHeader ({ theme, width }) {
  const isXsDown = isWidthDown('xs', width);

  return (
    <div
      className={css`
        display: flex;
        justify-content: space-between;
        color: ${theme.palette.primary.contrastText};
        height: 55px;
        padding: 0 ${isXsDown ? '10px' : '25px'};
        align-items: center;
      `}
    >
      <TextLink to="/">
        <AppLogo
          className={css`
            font-size: 32px;
            opacity: 0.6;
          `}
          withText
        />
      </TextLink>

      <AppHeaderMenu />
    </div>
  );
});
