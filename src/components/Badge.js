import Chip from '@material-ui/core/Chip';
import React from 'react';

export default function Badge (props) {
  return !!props.label && <Chip {...props} />;
}
