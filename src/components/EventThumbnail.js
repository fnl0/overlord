import React from 'react';
import styled from 'react-emotion';

const EventThumbnailWrapper = styled('div')`
  background: url("${props => props.imageUrl}");
  background-size: contain;
  background-repeat: no-repeat;
  background-position: top;
  height: ${props => props.imageSize};
  width: ${props => props.imageSize};
`;

export default function EventThumbnail ({ className, imageUrl, imageSize }) {
  return <EventThumbnailWrapper {...{className, imageUrl, imageSize}} />;
}
