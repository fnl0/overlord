import { withTheme } from '@material-ui/core/styles';
import withWidth, { isWidthDown } from '@material-ui/core/withWidth';
import React from 'react';
import { css } from 'react-emotion';
import HomeEvents from './home/HomeEvents';
import HomeActions from './home/HomeActions';
import AppBarTemplate from './templates/AppBarTemplate';

const hoc = BaseComponent =>
  withWidth()(withTheme()(BaseComponent));

export default hoc(function Home ({ theme, width }) {
  const isXsDown = isWidthDown('xs', width);

  return (
    <AppBarTemplate>
      <div
        className={css`
          display: flex;
          flex-wrap: wrap-reverse;
          flex: 1;
          padding: 0 ${isXsDown ? '10px' : '25px'};

          > * {
            min-width: 250px;
            max-height: calc(100vh - 55px);
            margin-bottom: 20px;
          }

          > * + * {
            margin-left: 15px;
          }
        `}
      >
        <HomeEvents
          className={css`
            flex: 7;
          `}
        />

        <HomeActions
          className={css`
            flex: 3;
          `}
        />
      </div>
    </AppBarTemplate>
  );
});
