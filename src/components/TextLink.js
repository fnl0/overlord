import { Link } from 'react-router-dom';
import styled from 'react-emotion';

export default styled(Link)`
  color: inherit;
  text-decoration-line: none;
`;
