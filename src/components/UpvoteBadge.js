import PlayArrow from '@material-ui/icons/PlayArrow';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import styled, { css } from 'react-emotion';

const UpvoteBadgeWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;
`;

export default class UpvoteBadge extends React.Component {
  state = {
    hover: false,
  };

  onMouseEnter = (event) => {
    this.setState({ hover: true });

    if (this.props.onMouseEnter) {
      this.props.onMouseEnter(event);
    }
  };

  onMouseLeave = (event) => {
    this.setState({ hover: false });

    if (this.props.onMouseLeave) {
      this.props.onMouseLeave(event);
    }
  };

  onClick = (event) => {
    if (this.props.onClick) {
      this.props.onClick(event, this.props.value);
    }
  };

  render() {
    const { className, count } = this.props;
    const { hover } = this.state;
    const { onClick, onMouseEnter, onMouseLeave } = this;

    return (
      <UpvoteBadgeWrapper
        className={className}
        onClick={onClick}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
      >
        <PlayArrow
          className={css`
            transform: rotate(-90deg) scale(1, 1.3);
            margin-bottom: -5px;
          `}
          color={hover ? 'secondary' : 'action'}
        />

        <Typography
          variant="h5"
          className={css`font-weight: 300;`}
        >
          {count || 0}
        </Typography>

        <Typography variant="body2">VOTES</Typography>
      </UpvoteBadgeWrapper>
    );
  }
}
