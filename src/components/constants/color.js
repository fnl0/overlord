export const pickColor = (number) => {
  if (number < 6) {
    return "#E6E6FA";
  }
  if (number < 9) {
    return "#f7b8f4";
  }
  if (number < 50) {
    return "#9400D3";
  }
  if (number >= 50) {
    return "#000000";
  }
};
