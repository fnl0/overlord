import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Admin from './Admin'
import axios from 'axios';
import Paper from "@material-ui/core/Paper/Paper";
import FormControl from "@material-ui/core/FormControl/FormControl";
import InputLabel from "@material-ui/core/InputLabel/InputLabel";
import Input from "@material-ui/core/Input/Input";
import Button from "@material-ui/core/Button/Button";
import TextField from "@material-ui/core/TextField/TextField";
import { withRouter } from 'react-router-dom';
import { TOPICS } from '../../constants/constants';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  chartContainer: {
    marginLeft: -22,
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  textField: {
    width: 300,
  },
  formControl: {
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing.unit * 2,
  },
});

class Event extends React.Component {

  state = {
    title: 'APAC Hackathon 2018',
    speaker: 'John',
    start: '2018-11-24T10:30',
    end: '2018-11-24T12:00',
    topic: ''
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    axios
      .post(`${process.env.REACT_APP_BACKEND}/events`, this.state)
      .then(response => {
        const code = response.data.code;
        this.props.history.push(`/event-details/${code}`);
      })
      .catch(error => {
        console.log(error)
      })
  };

  render() {
    const {classes} = this.props;

    return (
      <Admin>
        <main className={classes.content}>
          <div className={classes.appBarSpacer}/>
          <Typography variant="h4" gutterBottom component="h2">
            Create Event
          </Typography>
          <Paper className={classes.paper}>
            <form className={classes.form} onSubmit={this.handleSubmit}>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="email">Event Title</InputLabel>
                <Input id="title" name="title" autoFocus value={this.state.title}
                       onChange={this.handleChange('title')}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <InputLabel htmlFor="speaker">Speaker</InputLabel>
                <Input id="speaker" name="speaker" value={this.state.speaker}
                       onChange={this.handleChange('speaker')}
                />
              </FormControl>
              <FormControl  className={classes.formControl} margin="normal" required>
                <InputLabel shrink htmlFor="topic-label-placeholder">
                  Topic
                </InputLabel>
                <Select
                  value={this.state.topic}
                  onChange={this.handleChange('topic')}
                  input={<Input name="age" id="topic-label-placeholder" />}
                  displayEmpty
                  name="topic"
                  className={classes.selectEmpty}
                >
                  { TOPICS.map(topic => <MenuItem key={topic} value={topic}>{topic}</MenuItem>)}
                </Select>
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <TextField
                  id="datetime-local"
                  label="Start Date and Time"
                  type="datetime-local"
                  value={this.state.start}
                  onChange={this.handleChange('start')}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </FormControl>
              <FormControl margin="normal" required fullWidth>
                <TextField
                  id="datetime-local"
                  label="End Date and Time"
                  type="datetime-local"
                  value={this.state.end}
                  onChange={this.handleChange('end')}
                  className={classes.textField}
                  InputLabelProps={{
                    shrink: true,
                  }}
                />
              </FormControl>
              <Button type="submit" fullWidth variant="contained" color="primary" className={classes.submit}>
                Create Event
              </Button>
            </form>
          </Paper>
        </main>
      </Admin>
    );
  }
}

Event.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withRouter(withStyles(styles)(Event));
