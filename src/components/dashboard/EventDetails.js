import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Admin from './Admin'
import axios from 'axios';
import Paper from "@material-ui/core/Paper/Paper";
import QRCode from 'qrcode.react';
import Button from "@material-ui/core/Button/Button";
import Chip from '@material-ui/core/Chip';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';


const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  printButton: {
    marginBottom: theme.spacing.unit * 3,
  },
  labelCell: {
    width: 200,
  },
});

class EventDetails extends React.Component {

  state = {};

  componentDidMount() {
    axios.get(`${process.env.REACT_APP_BACKEND}/events?code=${this.props.match.params.code}`)
      .then(response => {
        this.setState(response.data.data[0])
      })
      .catch(error => {
        console.log(error);
      })
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    axios.post(`${process.env.REACT_APP_BACKEND}/events`,
      this.state,
    )
      .then(response => {
        this.setState({code: response.data.code})
      })
      .catch(error => {
        console.log(error)
      })
  };

  updateOngoing = ongoing => event => {
    event.preventDefault();
    axios
      .patch(`${process.env.REACT_APP_BACKEND}/events/${this.state._id}`, { ongoing })
      .then(response => {
        this.setState(response.data);
      });
  }

  render() {
    const {classes} = this.props;
    const {title, speaker, topic, code, ongoing, start, end} = this.state;
    const url = `https://fnl0.com/client?eventCode=${code}`;

    return (
      <Admin>
        <main className={classes.content}>
          <div className={classes.appBarSpacer}/>
          <Typography variant="h4" gutterBottom component="h2">
            Event Details <span>{ongoing ? <Chip color="primary" label="Active"/> : <Chip label="Inactive"/>}</span>
          </Typography>
          <Button color="primary" className={classes.printButton} variant="contained">Print</Button>
          &nbsp;
          {
            ongoing
              ? <Button color="primary" className={classes.printButton} variant="contained" onClick={this.updateOngoing(false)}>End Now</Button>
              : <Button color="primary" className={classes.printButton} variant="contained" onClick={this.updateOngoing(true)}>Start Now</Button>
          }

          <Paper className={classes.paper}>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell className={classes.labelCell}>Title</TableCell>
                  <TableCell>{title}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.labelCell}>Speaker</TableCell>
                  <TableCell>{speaker}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.labelCell}>Topic</TableCell>
                  <TableCell>{topic}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.labelCell}>Start</TableCell>
                  <TableCell>{start}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell className={classes.labelCell}>End</TableCell>
                  <TableCell>{end}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Paper>
          <br/>
          <Paper className={classes.paper}>
            <Typography variant="h5">
              Visit <a href={url} target="_blank" rel="noopener noreferrer">{url}</a> to join the event or scan this QR code:
            </Typography>
            <br/>
            <QRCode value={url} renderAs="svg" size={300}/>
          </Paper>
        </main>
      </Admin>
    );
  }
}

EventDetails.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EventDetails);
