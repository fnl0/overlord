import React from 'react';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
};

function EventTable(props) {
  const { classes, events } = props;
  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
            <TableCell>Title</TableCell>
            <TableCell>QR Code</TableCell>
            <TableCell>Speaker</TableCell>
            <TableCell>Start</TableCell>
            <TableCell>End</TableCell>
            <TableCell>Code</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {events.map(event => {
            return (
              <TableRow key={event._id}>
                <TableCell component="th" scope="row">
                  <Link to={'/view'}>{event.title}</Link>
                </TableCell>
                <TableCell component="th" scope="row">
                  <Link to={`/event-details/${event.code}`}>QR</Link>
                </TableCell>
                <TableCell>{event.speaker}</TableCell>
                <TableCell>{event.start}</TableCell>
                <TableCell>{event.end}</TableCell>
                <TableCell>{event.code}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

EventTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(EventTable);
