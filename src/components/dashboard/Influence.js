import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {ForceGraphLink, ForceGraphNode, InteractiveForceGraph} from 'react-vis-force';
import Admin from "./Admin";
import axios from "axios";
import _ from 'lodash';
import Tabs from "@material-ui/core/Tabs/Tabs";
import Tab from "@material-ui/core/Tab/Tab";
import Paper from "@material-ui/core/Paper/Paper";
import {pickColor} from "../constants/color";

const styles = theme => ({
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    height: '100vh',
    overflow: 'auto',
  },
});

const getUsers = () => {
  return axios.get('/api/users?$limit=500');
};

const getProfiles = () => {
  return axios.get('/api/profiles?$limit=10000');
};

function LinkTab(props) {
  return <Tab component="a" onClick={event => event.preventDefault()} {...props} />;
}

class Influence extends React.Component {

  state = {
    users: [], profiles: [], influenceIndex: {}, value: 0
  };

  handleChange = (event, value) => {
    this.setState({value});
  };

  componentDidMount() {
    axios.all([getUsers(), getProfiles()])
      .then(axios.spread((users, profiles) => {
        this.setState(
          {
            users: users.data.data,
            profiles: profiles.data.data,
          })
      }));
  }

  renderGraph = (topic) => {
    const {users, profiles} = this.state;
    return (
      <InteractiveForceGraph
        highlightDependencies
        simulationOptions={{animate: true, strength: {collide: 8}}}
        showLabels
        zoom
        zoomOptions={{minScale: 0.25, maxScale: 5}}>
        {
          users.map((user) => {
            let influenceIndex = _.countBy(profiles.filter(profile => profile.topic === topic), 'target');
            let radius = (influenceIndex[user.name] !== undefined) ? influenceIndex[user.name] * 1.5 : 5;
            return (
              <ForceGraphNode key={user._id} node={{id: user.name, radius: radius}} fill={pickColor(radius)}/>)
          })
        }
        {
          profiles.filter(profile => profile.topic === topic).map((profile) => {
            return (<ForceGraphLink key={profile._id}
                                         link={{source: profile.source, target: profile.target, value: 2}}/>)
          })
        }
      </InteractiveForceGraph>
    )
  };

  render() {
    const {classes} = this.props;
    const {users, profiles, value} = this.state;

    if (users.length === 0) {
      return <Admin>Wait for it...</Admin>
    }
    return (
      <Admin>
        <main className={classes.content}>
          <div className={classes.appBarSpacer}/>
          <Paper>
            <Tabs fullWidth value={value} onChange={this.handleChange}>
              <LinkTab label="Overall" href="overall"/>
              <LinkTab label="Frontend" href="frontend"/>
              <LinkTab label="Backend" href="backend"/>
              <LinkTab label="DevOps" href="devops"/>
              <LinkTab label="Design" href="design"/>
              <LinkTab label="Toolchain" href="toolchain"/>
            </Tabs>
            {(value === 0) &&
            <InteractiveForceGraph
              highlightDependencies
              simulationOptions={{animate: true, strength: {collide: 8}}}
              showLabels
              zoom
              zoomOptions={{minScale: 0.25, maxScale: 5}}>
              {
                users.map((user) => {
                  let influenceIndex = _.countBy(profiles, 'target');
                  let radius = (influenceIndex[user.name] !== undefined) ? influenceIndex[user.name] * 1.5 : 5;
                  return (
                    <ForceGraphNode key={user._id} node={{id: user.name, radius: radius}} fill={pickColor(radius)}/>)
                })
              }
              {
                profiles.map((profile) => {
                  return (<ForceGraphLink key={profile._id}
                                               link={{source: profile.source, target: profile.target, value: 2}}/>)
                })
              }
            </InteractiveForceGraph>
            }
            {value === 1 && this.renderGraph('Frontend')}
            {value === 2 && this.renderGraph('Backend')}
            {value === 3 && this.renderGraph('DevOps')}
            {value === 4 && this.renderGraph('Design')}
            {value === 5 && this.renderGraph('Toolchain')}
          </Paper>
        </main>
      </Admin>
    )
  }
}

Influence.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Influence);
