import React from 'react';
import ResponsiveContainer from 'recharts/lib/component/ResponsiveContainer';
import LineChart from 'recharts/lib/chart/LineChart';
import Line from 'recharts/lib/cartesian/Line';
import XAxis from 'recharts/lib/cartesian/XAxis';
import YAxis from 'recharts/lib/cartesian/YAxis';
import Tooltip from 'recharts/lib/component/Tooltip';
import Legend from 'recharts/lib/component/Legend';
import moment from 'moment';
import ReferenceLine from "recharts/lib/cartesian/ReferenceLine";

const chartData = [
  {excited: 50, agree: 50, time: 1542853800000},
  {excited: 70, agree: 80, time: 1542854400000},
  {excited: 100, agree: 80, time: 1542855000000},
  {excited: 20, agree: 50, time: 1542855600000},
  {excited: 90, agree: 70, time: 1542856200000},
];

class SimpleLineChart extends React.Component {

  render() {
    return (
      <React.Fragment>
        <ResponsiveContainer width="99%" height={200}>
          <LineChart data={chartData} syncId="anyId">
            <XAxis
              dataKey='time'
              domain={['auto', 'auto']}
              name='Time'
              tickFormatter={(unixTime) => moment(unixTime).format('HH:mm')}
              type='number'
            />
            <YAxis/>
            <Tooltip/>
            <Legend/>
            <ReferenceLine y="50" stroke="green" strokeDasharray="3 3"/>
            <Line type="monotone" dataKey="excited" stroke="#8884d8"/>
          </LineChart>
        </ResponsiveContainer>
        <ResponsiveContainer width="99%" height={200}>
          <LineChart data={chartData} syncId="anyId">
            <XAxis
              dataKey='time'
              domain={['auto', 'auto']}
              name='Time'
              tickFormatter={(unixTime) => moment(unixTime).format('HH:mm')}
              type='number'
            />
            <YAxis/>
            <Tooltip/>
            <Legend/>
            <ReferenceLine y="50" stroke="green" strokeDasharray="3 3"/>
            <Line type="monotone" dataKey="agree" stroke="#82ca9d"/>
          </LineChart>
        </ResponsiveContainer>
      </React.Fragment>
    );
  }
}

export default SimpleLineChart;
