import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import TrendingUpIcon from '@material-ui/icons/TrendingUp';
import {pickColor} from "../constants/color";
import Dialog from "@material-ui/core/Dialog/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle/DialogTitle";
import {PolarAngleAxis, PolarGrid, PolarRadiusAxis, Radar, RadarChart} from "recharts";
import DialogContent from "@material-ui/core/DialogContent/DialogContent";
import DialogActions from "@material-ui/core/DialogActions/DialogActions";
import Button from "@material-ui/core/Button/Button";

const styles = {
  root: {
    width: '100%',
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  avatar: {
    width: 45,
    height: 45,
  },
};

class RadarChartDiag extends React.Component {

  render() {
    const { open, handleClose, profile } = this.props;
    debugger
    return (
      <Dialog open={open} onClose={this.handleClose} aria-labelledby="simple-dialog-title">
        <DialogTitle id="simple-dialog-title">Influence over topics</DialogTitle>
        <DialogContent>
          <RadarChart cx={300} cy={250} outerRadius={150} width={600} height={500} data={profile}>
            <PolarGrid />
            <PolarAngleAxis dataKey="topic" />
            <PolarRadiusAxis/>
            <Radar name="User" dataKey="value" stroke="#8884d8" fill="#8884d8" fillOpacity={0.6}/>
          </RadarChart>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    );
  }
}

class UserTable extends React.Component {

  state = {
    open: false,
  };

  handleClickOpen = (profile) => {
    this.setState({ open: true, profile: profile });
  };

  handleClose = () => {
    this.setState({ open: false});
  };

  render() {

    const {classes, users, profiles} = this.props;
    const {open, profile} = this.state;

    return (
      <Paper className={classes.root}>
        <RadarChartDiag profile={profile} open={open} handleClose={this.handleClose}/>
        <Table className={classes.table} padding={'dense'}>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Influence Index</TableCell>
              <TableCell>Frontend</TableCell>
              <TableCell>Backend</TableCell>
              <TableCell>DevOps</TableCell>
              <TableCell>Toolchain</TableCell>
              <TableCell>Design</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {
              users.map(user => {
                const index = profiles.filter(profile => profile.target === user.name);
                const frontend = profiles.filter(profile => profile.topic === 'Frontend').filter(profile => profile.target === user.name);
                const backend = profiles.filter(profile => profile.topic === 'Backend').filter(profile => profile.target === user.name);
                const devops = profiles.filter(profile => profile.topic === 'DevOps').filter(profile => profile.target === user.name);
                const toolchain = profiles.filter(profile => profile.topic === 'Toolchain').filter(profile => profile.target === user.name);
                const design = profiles.filter(profile => profile.topic === 'Design').filter(profile => profile.target === user.name);
                const profile = [
                  { topic: "Frontend", value: frontend.length },
                  { topic: "Backend", value: backend.length },
                  { topic: "DevOps", value: devops.length },
                  { topic: "Toolchain", value: toolchain.length },
                  { topic: "Design", value: design.length },
                ];
                return (
                  <TableRow key={user._id}>
                    <TableCell component="th" scope="row">
                      <div onClick={() => this.handleClickOpen(profile)}>{user.name}</div></TableCell>
                    <TableCell style={{color: pickColor(index)}}><TrendingUpIcon/></TableCell>
                    <TableCell style={{color: pickColor(frontend)}}><TrendingUpIcon/></TableCell>
                    <TableCell style={{color: pickColor(backend)}}><TrendingUpIcon/></TableCell>
                    <TableCell style={{color: pickColor(devops)}}><TrendingUpIcon/></TableCell>
                    <TableCell style={{color: pickColor(toolchain)}}><TrendingUpIcon/></TableCell>
                    <TableCell style={{color: pickColor(design)}}><TrendingUpIcon/></TableCell>
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

UserTable.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserTable);
