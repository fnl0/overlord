import Button from '@material-ui/core/Button';
import { MuiThemeProvider } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField'
import Typography from '@material-ui/core/Typography';
import React from 'react';
import styled, { css } from 'react-emotion';
import { withRouter } from 'react-router-dom';
import { darkTheme } from '../../constants/themes';

const HomeActionsWrapper = styled('div')`
  align-items: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export default withRouter(class HomeActions extends React.Component {
  state = {
    eventCode: '',
  };

  inputRef = React.createRef();

  onEventCodeChange = (event) => {
    this.setState({ eventCode: event.target.value });
  };

  onJoin = () => {
    const eventCode = this.state.eventCode.trim();
    if (eventCode) {
      // Should redirect to correct event page
      this.props.history.push(`/client?eventCode=${eventCode}`);
    }
    else {
      this.inputRef.current.focus();
    }
  };

  onSubmit = (event) => {
    event.preventDefault();
    this.onJoin();
  };

  render() {
    const { className } = this.props;
    const { eventCode } = this.state;
    const { inputRef, onEventCodeChange, onJoin, onSubmit } = this;

    return (
      <HomeActionsWrapper className={className}>
        <MuiThemeProvider theme={darkTheme}>
          <Typography variant="h6" color="textPrimary"></Typography>

          <div
            className={css`
              margin-bottom: 20px;

              > * + * {
                margin-top: 5px;
              }
            `}
          >
            {[
              { text: 'Get your questions answered' },
              { text: 'Vote on important issues' },
            ].map(({ text }) => (
              <div key={text}>
                <Typography variant="h6" color="textPrimary">{text}</Typography>
              </div>
            ))}
          </div>

          <form
            className={css`
              display: flex;
              align-items: center;
            `}
            onSubmit={onSubmit}
          >
            <TextField
              className={css`
                margin-right: 10px;
              `}
              label="Event Code"
              variant="filled"
              inputRef={inputRef}
              onChange={onEventCodeChange}
              value={eventCode}
            />

            <Button
              className={css`
                margin-left: 10px;
              `}
              variant="extendedFab"
              color="primary"
              onClick={onJoin}
            >
              Join
            </Button>
          </form>
        </MuiThemeProvider>
      </HomeActionsWrapper>
    );
  }
});
