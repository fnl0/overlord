import Typography from '@material-ui/core/Typography';
import React from 'react';
import TextLink from '../TextLink';
import styled, { css } from 'react-emotion';

const HomeFooterItem = styled(TextLink)`
  margin-right: 1rem;
`;

export default function HomeFooter () {
  return (
      <nav
        className={css`
          display: flex;
          justify-content: center;
          padding: 10px 0;
          background: #fff;
        `}
      >
        <Typography color="primary">
          <HomeFooterItem to="/about">About</HomeFooterItem>
        </Typography>
      </nav>
  );
}
