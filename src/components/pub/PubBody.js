import { MuiThemeProvider } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import React from 'react';
import { css } from 'react-emotion';
import { defaultMemoize }from 'reselect';
import { darkTabsTheme } from '../../constants/themes';
import { subscribeService } from '../../utils/api';
import Badge from '../Badge';
import PubQuestions from './PubQuestions';
import PubPolls from './PubPolls';
import PubPeople from './PubPeople';
import { sortBy } from 'lodash';

const TABS = [
  {
    label: 'Ask a Question',
    Component: PubQuestions,
  },
  {
    label: 'Polls',
    Component: PubPolls,
  },
  {
    label: 'People',
    Component: PubPeople,
  },
];


export default class PubBody extends React.Component {
  state = {
    activeTab: 0,
    questions: null,
  };

  questionsSelector = defaultMemoize(
    (items, eventCode) => sortBy(
      Object.keys(items.byId)
        .map(id => items.byId[id])
        .filter(item => eventCode === item.eventCode),
      [item => item.upvotes * -1, item => Date.parse(item.createdAt).valueOf()],
  ));

  componentDidMount() {
    this.subscription = subscribeService('questions', data => {
      this.setState({
        questions: this.questionsSelector(data, this.props.event.code),
      });
    });
  }

  componentWillUnmount() {
    this.subscription.unsubscribe();
  }

  onChangeTab = (event, value) => {
    this.setState({ activeTab: value });
  };

  render() {
    const { event, width } = this.props;
    const { activeTab, questions } = this.state;
    const { onChangeTab } = this;

    return (
      <React.Fragment>
        <MuiThemeProvider theme={darkTabsTheme}>
          <Tabs
            value={activeTab}
            onChange={onChangeTab}
            indicatorColor="primary"
            textColor="primary"
            centered
          >
            {TABS.map(({ label }) => {
              let badgeLabel;
              switch (label) {
                case 'Ask a Question':
                  badgeLabel = (questions || []).length;
                  break;
                case 'People':
                  badgeLabel = 3;
                  break;
                default: badgeLabel = 0;
              }
              return (
                <Tab
                  key={label}
                  label={
                    <span>
                      {label}
                      <Badge className={css`margin-left: 5px;`} label={badgeLabel} />
                    </span>
                  }
                />
              );
            })}
          </Tabs>
        </MuiThemeProvider>

        {TABS.map(({ label, Component }, i) => activeTab === i && (
          <Component
            className={css`margin-bottom: 70px;`}
            key={label}
            questions={questions}
            width={width}
            event={event}
          />
        ))}
      </React.Fragment>
    );
  }
}
