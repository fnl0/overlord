import Typography from '@material-ui/core/Typography';
import { isWidthDown } from '@material-ui/core/withWidth';
import React from 'react';
import styled, { css } from 'react-emotion';
import EventThumbnail from '../EventThumbnail';
import PubProgress from './PubProgress';

const PubHeaderWrapper = styled('div')`
  display: flex;
  flex-direction: ${props => props.isXsDown ? 'column' : 'row'};
  align-items: ${props => props.isXsDown ? 'center' : 'flex-start'};
`;

export default function PubHeader ({ className, event, width }) {
  const isXsDown = isWidthDown('xs', width);

  return (
    <div className={className}>
      <PubHeaderWrapper isXsDown={isXsDown}>
        <EventThumbnail imageSize="80px" imageUrl={event.image} />

        <div
          className={css`
            margin-left: ${isXsDown ? '0' : '15px'};
            align-items: ${isXsDown ? 'center' : 'flex-start'};
            display: flex;
            flex-direction: column;
          `}
        >
          <Typography variant="h6">{event.title}</Typography>
          <Typography variant="subtitle1">By {event.speaker}</Typography>
        </div>
      </PubHeaderWrapper>

      <PubProgress
        className={css`
          margin-top: 10px;
          width: 100%;
        `}
        event={event}
      />
    </div>
  );
}
