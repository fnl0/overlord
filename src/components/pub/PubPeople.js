import ListItem from "@material-ui/core/ListItem/ListItem";
import React from "react";
import {defaultTheme} from "../../constants/themes";
import Card from "@material-ui/core/Card/Card";
import {css} from "emotion";
import List from "@material-ui/core/List/List";
import Typography from "@material-ui/core/Typography/Typography";
import {MuiThemeProvider} from "@material-ui/core";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';

export default function PubPeople() {

  return (
    <MuiThemeProvider theme={defaultTheme}>
      <Card>
        <List>
          <ListItem>
            <AccountCircleIcon/>
            <Typography
              variant="h6"
              className={css`
                      font-weight: 400;
                      margin-left: 10px;
                    `}>
              Max
            </Typography>
          </ListItem>
          <ListItem>
            <AccountCircleIcon/>
            <Typography
              variant="h6"
              className={css`
                      font-weight: 400;
                      margin-left: 10px;
                    `}>
              Lee
            </Typography>
          </ListItem>
          <ListItem>
            <AccountCircleIcon/>
            <Typography
              variant="h6"
              className={css`
                      font-weight: 400;
                      margin-left: 10px;
                    `}>
              Paul
            </Typography>
          </ListItem>
        </List>
      </Card>
    </MuiThemeProvider>
  )
}
