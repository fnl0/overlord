import Card from '@material-ui/core/Card';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { isWidthDown } from '@material-ui/core/withWidth';
import React from 'react';
import { css, cx } from 'react-emotion';
import { defaultTheme } from '../../constants/themes';

import Mood from '@material-ui/icons/Mood';
import MoodBad from '@material-ui/icons/MoodBad';
import ClickNHold from 'react-click-n-hold'; 
// eslint-disable-next-line
import cnhcss from '../../assets/cnh.css';
import Typography from '@material-ui/core/Typography';

export default class PubQuestions extends React.Component {

    state = { message: 'Please cast your vote by expressing your emotions....' };
    start = (e) => {
      console.log('START'); 
    } 
    end = (e, enough) => {
       console.log('END'); 
       console.log(enough ? 'Click released after enough time': 'Click released too soon'); 
       const message = enough ? 'You have gave a BIG GOOD!' : 'You have gave a GOOD!';
       this.setState({message});
     } 

    clickNHold = (e) => {
       console.log('CLICK AND HOLD'); 
       this.setState({message: 'You have gave a BIG GOOD!'});
    } 

    endBad = (e, enough) => {
       console.log('END MEH'); 
       console.log(enough ? 'Click released after enough time': 'Click released too soon'); 
       const message = enough ? 'You have gave a BIG MEH!!!!' : 'You have gave a MEH!!!!';
       this.setState({message});
    } 
    
    clickNHoldBad = (e) => {
        console.log('CLICK AND HOLD MEH'); 
        this.setState({message: 'You have gave a BIG MEH!!!!'});
    } 
    
    render() {
      const { className, width } = this.props;
      const isXsDown = isWidthDown('xs', width);
  
      return (
        <MuiThemeProvider theme={defaultTheme}>
          <Card className={cx(css`padding: ${isXsDown ? '10px 5px' : '20px 10px'}; height: 300px;`, className)}>
            <div className="parent" style={{ display: 'inline-block', width: '50%' }}>
              <div className="child">
                <ClickNHold 
                    time={2} 
                    onStart={this.start} 
                    onClickNHold={this.clickNHold} 
                    onEnd={this.end}
                >
                    <div id='mood'>
                    <Mood fontSize='inherit'/>
                    </div>
                </ClickNHold>
              </div>
            </div>
            <div className="parent" style={{ display: 'inline-block', width: '50%' }}>
              <div className="child">
                <ClickNHold 
                    time={2} 
                    onStart={this.start} 
                    onClickNHold={this.clickNHoldBad} 
                    onEnd={this.endBad}
                >
                    <div id='mood'>
                    <MoodBad fontSize='inherit'/>
                    </div>
                </ClickNHold>
              </div>
            </div>
          </Card>
          <Card>
            <Typography
              variant="h6"
              className={css`
                font-weight: 400;
                 margin-left: 10px;
               `}
            >
              {this.state.message}
            </Typography>
          </Card>
        </MuiThemeProvider>
      );
    }
  }
