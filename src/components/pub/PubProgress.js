import LinearProgress from '@material-ui/core/LinearProgress';
import Typography from '@material-ui/core/Typography';
import moment from 'moment';
import React from 'react';
import { css } from 'react-emotion';
import { formatDuration, parseEventDate } from '../../utils/dates';

export default class PubProgress extends React.Component {
  state = {
    value: 0,
    started: false,
  };

  componentDidMount() {
    this.interval = setInterval(() => {
      const { event: { start, end } } = this.props;
      const startValue = parseEventDate(start).valueOf();
      const endValue = parseEventDate(end).valueOf();
      const nowValue = moment().valueOf();
      const elapsed =  nowValue - startValue;
      const total = endValue - startValue;

      // value = (now - start) / (end - start)
      this.setState({
        total,
        elapsed,
        value: (elapsed / total * 100),
        started: nowValue >= startValue,
      });
    }, 1000);
  }

  componentWillUnmount() {
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  render() {
    const { className } = this.props;
    const { value, elapsed, total } = this.state;

    return (
      <div className={className}>
        <LinearProgress
          className={css`width: 100%`}
          variant="determinate"
          value={value}
        />

        <div
          className={css`width: 100%`}
        >
          <div
            className={css`
              display: flex;
              justify-content: space-between;
            `}
          >
            <Typography variant="caption">
              {formatDuration(moment.duration(elapsed))}
            </Typography>

            <Typography variant="caption">
              {formatDuration(moment.duration(total))}
            </Typography>
          </div>
        </div>
      </div>
    );
  }
}
