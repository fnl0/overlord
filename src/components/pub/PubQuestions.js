import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { MuiThemeProvider } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { isWidthDown } from '@material-ui/core/withWidth';
import React from 'react';
import { css, cx } from 'react-emotion';
import { defaultTheme } from '../../constants/themes';
import { addQuestion, upvoteQuestion } from '../../utils/api';
import UpvoteBadge from '../UpvoteBadge';

export default class PubQuestions extends React.Component {
  askInputRef = React.createRef();

  state = {
    isAsking: false,
    askInput: '',
    upvotesByQnId: {}, // workaround until we get user's id
  };

  onAsk = () => {
    this.setState({ isAsking: true }, () => {
      this.askInputRef.current.focus();
    });
  };

  onAskBlur = () => {
    setTimeout(() => {
      this.setState({ isAsking: false });
    }, 100);
  };

  onAskKeyPress = (event) => {
    if(event.keyCode === 27/* esc */) {
      this.onAskBlur();
    }
    else if (event.keyCode === 13/* esc */) {
      event.preventDefault();
      this.onAskSubmit();
    }
  };

  onAskInputChange = (event) => {
    this.setState({ askInput: event.target.value });
  };

  onAskSubmit = (event) => {
    const askInput = this.state.askInput.trim();

    if (askInput) {
      addQuestion(askInput, { eventCode: this.props.event.code })
        .then(() => {
          this.onAskBlur();
          this.setState({ askInput: '' });
        })
        .catch(console.error);
    }
  };

  onUpvote = (event, value) => {
    if (this.state.upvotesByQnId[value._id]) {
      return;
    }

    this.setState({
      upvotesByQnId: { ...this.state.upvotesByQnId, [value._id]: true }
    });
    upvoteQuestion(value);
  };

  render() {
    const { className, width, questions } = this.props;
    const { isAsking, askInput } = this.state;
    const {
      askInputRef,
      onAsk,
      onAskBlur,
      onAskKeyPress,
      onAskInputChange,
      onAskSubmit,
      onUpvote,
    } = this;
    const isXsDown = isWidthDown('xs', width);

    return (
      <MuiThemeProvider theme={defaultTheme}>
        <Card className={cx(css`padding: ${isXsDown ? '10px 5px' : '20px 10px'};`, className)}>
          <div className={css`display: flex; justify-content: center; margin-bottom: 20px;`}>
            {isAsking
              ? (
                <form
                  className={css`
                    align-items: center;
                    display: flex;
                    flex-direction: row;
                    min-width: 65%;
                  `}
                >
                  <TextField
                    className={css`min-width: 65%; margin-right: 10px;`}
                    inputRef={askInputRef}
                    multiline
                    onBlur={onAskBlur}
                    onChange={onAskInputChange}
                    onKeyDown={onAskKeyPress}
                    value={askInput}
                  />
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={onAskSubmit}
                  >
                    Submit
                  </Button>
                </form>
              )
              : (
                <Button
                  variant="contained"
                  color="secondary"
                  className={css`min-width: 65%;`}
                  onClick={onAsk}
                >
                  Ask a question or suggest a topic
                </Button>
              )
            }
          </div>

          <Divider />

          <List>
            {(questions || []).map((question, i) => {
              const { _id, content, upvotes } = question;

              return (
                <ListItem
                  key={_id}
                  divider={i < (questions || []).length - 1}
                >
                  <UpvoteBadge count={upvotes} onClick={onUpvote} value={question} />

                  <Typography
                    variant="h6"
                    className={css`
                      font-weight: 400;
                      margin-left: 10px;
                    `}
                  >
                    {content}
                  </Typography>
                </ListItem>
              );
            })}
          </List>
        </Card>
      </MuiThemeProvider>
    );
  }
}
