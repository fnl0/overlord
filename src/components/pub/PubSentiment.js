import Typography from '@material-ui/core/Typography';
import React from 'react';
import { css } from 'react-emotion';
import { config, Spring } from 'react-spring';

const EMOTIONS = [
  {
    label: 'Excited',
    icon: '😀',
  },
  {
    label: 'Mehh',
    icon: '😐',
  }
];

export default class PubSentiment extends React.Component {
  state = {
    emotion: null,
    isClicking: true
  };

  onClickButton = (emotion) => (event) => {
    this.setState({ emotion, isClicking: true });
  };

  onUnclickButton = (event) => {
    this.setState({ emotion: null, isClicking: false });
  };

  render() {
    const { emotion, isClicking } = this.state;
    const { onClickButton, onUnclickButton } = this;

    return (
      <React.Fragment>
        <div
          className={css`
            display: flex;
            position: fixed;
            bottom: 0;
            left: 0;
            right: 0;
            height: 55px;
            background: #eee;
          `}
        >
          {EMOTIONS.map(({ label, icon }, i) => (
            <div
              key={label}
              className={css`
                flex: 1;
                display: flex;
                align-items: center;
                justify-content: center;
                cursor: pointer;
                user-select: none;
              `}
              onTouchStart={onClickButton(i)}
              onTouchEnd={onUnclickButton}
              onMouseDown={onClickButton(i)}
              onMouseLeave={onUnclickButton}
              onMouseUp={onUnclickButton}
            >
              <span className={css`font-size: 25px`}>{icon}</span>
              <Typography variant="h6">{label}</Typography>
            </div>
          ))} 
        </div>


        <Spring
          config={config.slow}
          from={{ fontSize: '0px' }}
          to={{ fontSize: '200px' }}
          onRest={onUnclickButton}
          delay={400}
          reset={!isClicking}
        >
          {props =>
            <div
              className={css`
                position: fixed;
                top: 50%;
                left: 50%;
                transform: translate(-50%, -50%);
              `}
              style={props}
            >
              {(EMOTIONS[emotion] || {}).icon}
            </div>
          }
        </Spring>
      </React.Fragment>
    );
  }
}
