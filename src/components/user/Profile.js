import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import {Redirect} from 'react-router'
import axios from "axios";
import {Link} from "react-router-dom";
import Step from "@material-ui/core/Step/Step";
import StepLabel from "@material-ui/core/StepLabel/StepLabel";
import Stepper from "@material-ui/core/Stepper/Stepper";
import Grid from "@material-ui/core/Grid/Grid";
import SelectPerson from "./SelectPerson";
import _ from 'lodash';
import client from "../../feathers";

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  grid: {
    height: '70px'
  }
});

class Profile extends React.Component {

  state = {
    users: [],
    user: {},
    selections: [],
    done: false,
    loggedIn: false
  };

  componentDidMount() {
    client.authenticate().then(() => {
      debugger
      this.setState({loggedIn: true, user: client.get('user')})
    }).catch(() => {
      debugger
      this.setState({loggedIn: null})
    });
    axios.get(`${process.env.REACT_APP_BACKEND}/users?$sort[name]=1&$limit=100`)
      .then(response => {
        this.setState({users: response.data.data.map(user => { return {label : user.name, value: user.name  }})})
      })
      .catch(error => {
        console.log(error);
      })
  }

  handleSubmit = (event) => {
    event.preventDefault();
    const payload = _.uniqWith(this.state.selections, _.isEqual);
    console.log(payload);
    axios.post('/api/profiles', payload)
      .then(response => {
        this.setState({done: true})
      })
      .catch(error => {
        console.log(error)
      })
  };

  selections = (selections) => {
    this.setState({
      selections: this.state.selections.concat(selections),
    })
  };

  render() {
    const {classes} = this.props;
    const {done} = this.state;

    if (done) {
      return (<Redirect to="/done"/>)
    }
    return (
      <main className={classes.main}>
        <CssBaseline/>
        <Paper className={classes.paper}>
          <Stepper activeStep={1}>
            <Step key={'register'}>
              <StepLabel>Register</StepLabel>
            </Step>
            <Step key={'profile'}>
              <StepLabel>Profile</StepLabel>
            </Step>
            <Step key={'done'}>
              <StepLabel>Done</StepLabel>
            </Step>
          </Stepper>
          <Typography component="h1" variant="h5">
            Hi, {this.state.user.name}. Tell us about your influencers
          </Typography>
          <form className={classes.form} onSubmit={this.handleSubmit}>
            <Typography gutterBottom>
              These are the people that helps you make better decisions.
            </Typography>
            <Grid container spacing={24}>
              <Grid item xs={12} className={classes.grid}>
                <SelectPerson label={'Frontend'}
                              user={this.state.user.name}
                              suggestions={this.state.users}
                              selections={this.selections}/>
              </Grid>
              <Grid item xs={12} className={classes.grid}>
                <SelectPerson label={'Backend'}
                              user={this.state.user.name}
                              suggestions={this.state.users}
                              selections={this.selections}/>
              </Grid>
              <Grid item xs={12} className={classes.grid}>
                <SelectPerson label={'Design'}
                              user={this.state.user.name}
                              suggestions={this.state.users}
                              selections={this.selections}/>
              </Grid>
              <Grid item xs={12} className={classes.grid}>
                <SelectPerson label={'DevOps'}
                              user={this.state.user.name}
                              suggestions={this.state.users}
                              selections={this.selections}/>
              </Grid>
              <Grid item xs={12} className={classes.grid}>
                <SelectPerson label={'Toolchain'}
                              user={this.state.user.name}
                              suggestions={this.state.users}
                              selections={this.selections}/>
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}>
              Submit
            </Button>
          </form>
        </Paper>
        <Paper className={classes.paper}>
          <Button component={Link} to={"/"}>Return Home</Button>
        </Paper>
      </main>
    );
  }
}

Profile.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Profile);
