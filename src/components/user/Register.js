import React from 'react';
import PropTypes from 'prop-types';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import PortraitIcon from '@material-ui/icons/Portrait';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import {Redirect} from 'react-router'
import axios from "axios";
import {Link} from "react-router-dom";
import Step from "@material-ui/core/Step/Step";
import StepLabel from "@material-ui/core/StepLabel/StepLabel";
import Stepper from "@material-ui/core/Stepper/Stepper";
import client from '../../feathers';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  paper: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
});

class SignIn extends React.Component {

  state = {
    email: 'demo@fnl0.com', name: 'demo', password: 'secret', registered: false
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  handleSubmit = (event) => {
    event.preventDefault();
    axios.post('/api/users', this.state)
      .then(() => {
        client.authenticate({
          strategy: 'local',
          email: this.state.email,
          password: this.state.password
        })
          .then(response => {
            return client.passport.verifyJWT(response.accessToken);
          })
          .then(payload => {
            return client.service('api/users').get(payload.userId);
          })
          .then(user => {
            client.set('user', user);
            this.setState({registered: true})
          })
          .catch(function (error) {
            console.error('Error authenticating!', error);
          });
      })
      .catch(error => {
        this.setState({errorMessage: error.response.data.message})
      })
  };

  render() {
    const {classes} = this.props;
    const {registered, errorMessage} = this.state;

    if (registered) {
      return (<Redirect to="/profile"/>)
    }

    return (
      <main className={classes.main}>
        <CssBaseline/>
        <Paper className={classes.paper}>
          <Stepper activeStep={0} className={classes.stepper}>
            <Step key={'register'}>
              <StepLabel>Register</StepLabel>
            </Step>
            <Step key={'profile'}>
              <StepLabel>Profile</StepLabel>
            </Step>
            <Step key={'done'}>
              <StepLabel>Done</StepLabel>
            </Step>
          </Stepper>
          <Avatar className={classes.avatar}>
            <PortraitIcon/>
          </Avatar>
          <Typography component="h1" variant="h5">
            Register
          </Typography>
          <form className={classes.form} onSubmit={this.handleSubmit}>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="email">Email Address</InputLabel>
              <Input id="email" name="email" autoComplete="email" value={this.state.email}
                     onChange={this.handleChange('email')} autoFocus/>
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="name">Name</InputLabel>
              <Input id="name" name="name" value={this.state.name}
                     onChange={this.handleChange('name')}/>
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Password</InputLabel>
              <Input name="password" type="password" id="password" value={this.state.password}
                     onChange={this.handleChange('password')} autoComplete="current-password"/>
            </FormControl>
            <FormControlLabel
              control={<Checkbox value="remember" color="primary"/>}
              label="Remember me"
            />
            {errorMessage && <Typography>{errorMessage}</Typography>}
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}>
              Register
            </Button>
          </form>
        </Paper>
        <Paper className={classes.paper}>
          <Button component={Link} to={"/"}>Return Home</Button>
        </Paper>
      </main>
    );
  }
}

SignIn.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(SignIn);
