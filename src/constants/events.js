import moment from 'moment';
import img1 from '../assets/img1.jpg';
import img2 from '../assets/img2.jpg';
import img3 from '../assets/img3.jpg';
import img4 from '../assets/img4.jpg';
import img5 from '../assets/img5.jpg';
import img6 from '../assets/img6.jpg';

export const EVENTS = [
  {
    image: img1,
    title: "Introduction to Kubernetes",
    speaker: "Lee Cooper",
    start: moment().toISOString(),
    end: moment().add(1, 'h').toISOString(),
    code: "K8S1",
    createdAt: "2018-11-20T06:17:39.072Z",
    _id: "kM7NNsyP0Dsp4vyO1",
  },
  {
    image: img2,
    title: 'Kotlin in Production',
    speaker: 'YZ',
    start: moment().add(1, 'h').toISOString(),
    end: moment().add(2, 'h').toISOString(),
    code:"KOT1",
    createdAt:"2018-11-20T06:14:17.504Z",
    _id:"BAcGWjRvhlYz43Bt1",
  },
  {
    image: img3,
    title: 'Chat Bots',
    speaker: 'Zohar',
    start: moment().add(1, 'h').toISOString(),
    end: moment().add(2, 'h').toISOString(),
    code:"BOT1",
    createdAt:"2018-11-20T06:14:17.504Z",
    _id:"BAcGWjRvhlYz43Bt1",
  },
  {
    image: img4,
    title: 'Visualizing Financial Data',
    speaker: 'Ofer',
    start: moment().add(1, 'h').toISOString(),
    end: moment().add(2, 'h').toISOString(),
    code:"VFD1",
    createdAt:"2018-11-20T06:14:17.504Z",
    _id:"BAcGWjRvhlYz43Bt1",
  },
  {
    image: img5,
    title: 'Cloud Platform',
    speaker: 'Patrick',
    start: moment().add(1, 'd').toISOString(),
    end: moment().add(2, 'd').toISOString(),
    code:"C3P0",
    createdAt:"2018-11-20T06:14:17.504Z",
    _id:"BAcGWjRvhlYz43Bt1",
  },
  {
    image: img6,
    title: 'Progressive Web App',
    speaker: 'Noorul',
    start: moment().add(3, 'd').toISOString(),
    end: moment().add(4, 'd').toISOString(),
    code:"PWA1",
    createdAt:"2018-11-20T06:14:17.504Z",
    _id:"BAcGWjRvhlYz43Bt1",
  },
];
