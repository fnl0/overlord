const feathers = require('@feathersjs/feathers');
const socketio = require('@feathersjs/socketio-client');
const io = require('socket.io-client');
const auth = require('@feathersjs/authentication-client');

const BASE_WS = process.env.REACT_APP_WS;
const socket = io(BASE_WS);
const client = feathers();

if (process.env.NODE_ENV !== 'test') {
  // Setup the transport (Rest, Socket, etc.) here
  client.configure(socketio(socket));

  // Available options are listed in the "Options" section
  client.configure(auth())
}

export default client;
