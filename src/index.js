import CssBaseline from '@material-ui/core/CssBaseline';
import { MuiThemeProvider } from '@material-ui/core/styles';
import moment from 'moment';
import momentDurationFormat from 'moment-duration-format';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import 'typeface-roboto';
import { defaultTheme } from './constants/themes';
import AppRouter from './routes/AppRouter';
import './index.css';
import * as serviceWorker from './serviceWorker';
import { store } from './store/Store';

momentDurationFormat(moment);

ReactDOM.render((
 <Provider store={store}>
    <React.Fragment>
      <CssBaseline />
      <MuiThemeProvider theme={defaultTheme}>
        <AppRouter />
      </MuiThemeProvider>
    </React.Fragment>
  </Provider>
), document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.register();
