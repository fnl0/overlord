import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import Event from '../components/dashboard/Event';
import Pub from '../components/Pub';
import EventDetails from '../components/dashboard/EventDetails';
import Events from '../components/dashboard/Events';
import Home from '../components/Home';
import NotFound from '../components/NotFound';
import SignIn from "../components/user/SignIn";
import View from "../components/dashboard/View";
import Influence from "../components/dashboard/Influence";
import Users from "../components/dashboard/Users";
import Register from "../components/user/Register";
import Profile from "../components/user/Profile";
import Done from "../components/user/Done";

export default function AppRouter () {
  return (
    <Router basename={process.env.REACT_APP_BASE_NAME}>
      <Switch>
        <Route path="/event" component={Event} />
        <Route path="/influence" component={Influence} />
        <Route path="/users" component={Users} />
        <Route path="/register" component={Register} />
        <Route path="/done" component={Done} />
        <Route path="/profile" component={Profile} />
        <Route path="/client" component={Pub} />
        <Route path="/event-details/:code" component={EventDetails} />
        <Route path="/events" component={Events} />
        <Route path="/view" component={View} />
        <Route path="/sign-in" component={SignIn} />
        <Route path="/" exact component={Home} />
        <Route component={NotFound} />
      </Switch>
    </Router>
  );
}
