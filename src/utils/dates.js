import moment from 'moment';

export const parseEventDate = (eventDate) =>
  moment(eventDate, moment.ISO_8601);

export const formatEventDate = (eventDate) =>
  parseEventDate(eventDate).fromNow();

export const formatDuration = (duration) => {
  const str = duration.format('hh:mm:ss');

  return str.length === 2 ? `00:${str}`: str;
};
